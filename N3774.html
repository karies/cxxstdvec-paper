<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="generator" content="pandoc">
  <meta name="author" content="Axel Naumann (axel@cern.ch)">
  <meta name="author" content="Sandro Wenzel (sandro.wenzel@cern.ch)">
  <meta name="dcterms.date" content="2013-09-19">
  <title>C++ Needs Language Support For Vectorization ISO/IEC JTC1 SC22 WG21 N3774</title>
  <style type="text/css">code{white-space: pre;}</style>
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="markdown3.css">
</head>
<body>
<header>
<h1 class="title">C++ Needs Language Support For Vectorization ISO/IEC JTC1 SC22 WG21 N3774</h1>
<h2 class="author">Axel Naumann (axel@cern.ch)</h2>
<h2 class="author">Sandro Wenzel (sandro.wenzel@cern.ch)</h2>
<h3 class="date">2013-09-19</h3>
</header>
<nav id="TOC">
<ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#vectorization">Vectorization</a><ul>
<li><a href="#vectorization-and-loop-semantics">Vectorization and loop semantics</a></li>
<li><a href="#vectorization-versus-threading">Vectorization versus threading</a></li>
<li><a href="#types-of-vectorization">Types of vectorization</a></li>
</ul></li>
<li><a href="#performance-comparisons-of-different-vectorization-mechanisms">Performance comparisons of different vectorization mechanisms</a><ul>
<li><a href="#algorithm-used-for-performance-measurements">Algorithm used for performance measurements</a><ul>
<li><a href="#auto-vectorization">Auto-vectorization</a></li>
<li><a href="#vectorization-using-intrinsics">Vectorization using intrinsics</a></li>
</ul></li>
<li><a href="#language-enabled-vectorization">Language-enabled vectorization</a></li>
</ul></li>
<li><a href="#motivation-for-vectorization-in-c">Motivation for vectorization in C++</a><ul>
<li><a href="#use-past-current-and-future-hardware-efficiently-with-c">Use past, current and future hardware efficiently, with C++</a></li>
<li><a href="#vectorization-should-not-be-hidden-in-a-library">Vectorization should not be hidden in a library</a><ul>
<li><a href="#vectorized-library-components">Vectorized library components</a></li>
<li><a href="#vectorizing-library-components">Vectorizing library components</a></li>
<li><a href="#library-components-wrapping-vector-intrinsics">Library components wrapping vector intrinsics</a></li>
</ul></li>
<li><a href="#prevent-escape-to-non-standard-extensions">Prevent escape to non-standard extensions</a></li>
</ul></li>
<li><a href="#infrequently-used-counterarguments">Infrequently used counterarguments</a></li>
<li><a href="#conclusion">Conclusion</a></li>
<li><a href="#references">References</a></li>
</ul>
</nav>
<h1 id="introduction"><a href="#introduction">Introduction</a></h1>
<p>In this paper we argue that vectorization is a very different concept to parallelization and needs to be supported explicitly by the language. The lack of C++ support for explicit vectorization costs factors (2 to 4 in real code) of performance on current commodity hardware. We demonstrate why we believe vectorization is a much needed language feature. The arguments presented in this paper are based on, and accompanied by, performance measurements of code used at CERN.</p>
<h1 id="vectorization"><a href="#vectorization">Vectorization</a></h1>
<p>Operations can be reshuffled and combined into vector operations, replacing for instance a loop by a single operation on a consecutive set of data. As an example, the non-vectorized code</p>
<pre><code>int in0[4] = {0};
int in1[4] = {0, 1, 2, 3};
int out[4];
for (int i = 0; i &lt; 4; ++i)
  out[i] = in0[i] + in1[i];</code></pre>
<p>could be transformed into vectorized (pseudo-) code</p>
<pre><code>int in0[4];
vector_init(in0, 0);
int in1[4] = {0, 1, 2, 3};
int out[4];
vector_add(out, in0, in1);</code></pre>
<p>where <code>vector_xyz</code> are hypothetical, architecture-specific instructions (<em>vector intrinsics</em>) operating on a set of data.</p>
<h2 id="vectorization-and-loop-semantics"><a href="#vectorization-and-loop-semantics">Vectorization and loop semantics</a></h2>
<p>Vectorized operations do not guarantee the same order of iteration as traditional loops. If the original loop looked like this:</p>
<pre><code>int in0[4] = {0, 1, 2, 3};
int out[5] = {0};
for (int i = 0; i &lt; 4; ++i)
  out[i+1] = in0[i] + out[i];</code></pre>
<p>then the vectorized version of this example will generally not produce the same (nor even reproducible) result, because the sequencing of reading from and storing to <code>out</code> is likely going to be different than in a traditional loop. This is a fundamental semantic change of loops, likely violating 1.9 [intro.execution].</p>
<h2 id="vectorization-versus-threading"><a href="#vectorization-versus-threading">Vectorization versus threading</a></h2>
<p>High Energy Physics, with its embarrassingly parallel problems, has used multi-process parallelism for decades, both for multi-node (“cluster”) and single node parallelism. Additionally we selectively employ multithreading to cut down the initialization time and to reduce memory usage (shared memory of threads versus separate process memory). These two concurrency mechanisms are used to schedule tasks on processors; they do not increase the software’s efficiency on a single processor. Vectorization, on the other hand, does improve the efficiency (and in particular the throughput) of our software, by using the CPU’s vector resources.</p>
<p>Number crunching software, as developed by institutions like CERN, control the parallelization in a very direct way: process parallelism is used in combination with thread parallelization; having the compiler guess what should be good for us would at least be a wasted effort, if not counterproductive.</p>
<p>Also for developers, vector loops are conceptually <em>very</em> different from thread parallelization; see for instance <span class="citation" data-cites="N3735">(1)</span> contrasting these two. While thread parallelization encourages keeping data in disjoint memory regions and is closer in spirit to multi-process parallelization (with issues such as synchronization, starving, pooling etc), vectorization is a data-centric concept.</p>
<p>Thus, vectorization is orthogonal to parallelization and needs to be handled separately. A picture might contrast these two concepts even more clearly: large trucks are used to remove excavation material in mines. We currently use only a fraction of the size of the trucks; vectorization would allow us to use them to a larger extent. Threading is the concept of using multiple trucks – which is good but different, and not a good reason for running mostly empty trucks.</p>
<h2 id="types-of-vectorization"><a href="#types-of-vectorization">Types of vectorization</a></h2>
<p>In principle, we can distinguish three different kinds of vectorization:</p>
<dl>
<dt>Auto-Vectorization</dt>
<dd><p>This is the only type of compiler-generated vectorization available to standard compliant code. Because auto-vectorized and non-vectorized code must yield the same results, the compiler must guarantee that aliasing and order of operations with respect to value dependency cannot interfere with the result. Vector engines of most current hardware require a certain alignment of the groups of input data, that must be guaranteed by the compiler. Any non-vectorizable operation will prevent the compiler from auto-vectorizing code. All of this explains why only very few tight and simple loops with local-only data can possibly be auto-vectorized.</p>
</dd>
<dt>Vectorization through Vector Wrapping Intrinsics</dt>
<dd><p>An alternative to auto-vectorization is explicit vector-oriented programming. Several libraries exist or are suggested that hide the architecture specific vector intrinsics and expose them through a common interface; see for instance <span class="citation" data-cites="N3571 VC N3759">(2–4)</span>. Use of intrinsics can lead to significant performance boosts, at the cost of a fundamental change in code layout. In addition, they prevent certain compiler optimizations in code calling into these libraries; memory management and vectorization is defined by the library, which has no knowledge of the code using it.</p>
<p>The fact that the libraries are targeted to a specific set of architectures (which inevitably leaves traces in their interfaces, for instance for alignment) makes them a good <em>interim</em> option.</p>
</dd>
<dt>Vectorization Through Code Specifications</dt>
<dd><p>A third approach uses language extensions. Code can be annotated to guarantee to the compiler that the prerequisites for vectorizing it are met – even though (just as in the previous approach) the compiler cannot determine the correctness of this guarantee and thus would usually not auto-vectorize. Typical examples of the technique are vectorization pragmas <span class="citation" data-cites="MSCLPRAGMA INTELPRAGMA">(5, 6)</span>, OpenMP 4.0 <span class="citation" data-cites="OPENMP4">(7)</span> and Intel Cilk Plus <span class="citation" data-cites="IntelCilkPlus">(8)</span>; a proposal to the committee <span class="citation" data-cites="N3734">(9)</span> is similar to the latter.</p>
<p>Our favored approach, annotated loops as in <span class="citation" data-cites="ADVisualDSP INTELPRAGMA N3734">(6, 9, 10)</span>, is one of the key language extensions employing this technique. This programming model remains close to that of current C++ making it more accessible. As vectorization through annotations is controlled by the compiler, compiler optimizations can still be leveraged. We have seen cases where the compiler decided against the use of vectorization in annotated loops; a cross-check with explicit vectorization showed that this was indeed the correct optimization.</p>
<p>Compiler-generated vectorization can easily and naturally target the vectorization code for a specific architecture, the same way it currently targets non-vector code generation. With the emergence of CPU / GPU shared memory regions, targeting the vector code to GPUs will be an obvious next step that in fact has already started <span class="citation" data-cites="OPENMP4">(7)</span>.</p>
</dd>
</dl>
<h1 id="performance-comparisons-of-different-vectorization-mechanisms"><a href="#performance-comparisons-of-different-vectorization-mechanisms">Performance comparisons of different vectorization mechanisms</a></h1>
<h2 id="algorithm-used-for-performance-measurements"><a href="#algorithm-used-for-performance-measurements">Algorithm used for performance measurements</a></h2>
<p>For the purpose of this paper, we give an example from the field of detector simulation, which is one of the major consumers of CPU ressources at CERN. Already targeting multi-threading for some time <span class="citation" data-cites="Geant4MT">(11)</span>, current efforts focus on vectorization support within the simulation software <span class="citation" data-cites="GeantV">(12)</span> to increase its floating point performance. A very important part of such simulations are simple geometrical calculations, determining for instance whether a particle is inside a particular shape or calculating its distance to the shape’s surface. Similar operations are often done in the gaming industry.</p>
<p>As an illustrative example for such calculations we present the following small function that determines if a particle of coordinates <code>point[0] point[1] point[2]</code> is located inside a rectangular box of lateral width <code>boxsize[..]</code> whose origin is located at <code>origin</code>.</p>
<pre><code>bool contains(double const* point)
{
   bool inside[3];
   for(int dir=0;dir &lt; 3;dir++){
        inside[dir] = std::abs(point[dir]-origin[dir]) &lt; boxsize[dir]; 
   }
   return inside[0] &amp;&amp; inside[1] &amp;&amp; inside[2];
}</code></pre>
<h3 id="auto-vectorization"><a href="#auto-vectorization">Auto-vectorization</a></h3>
<p>In future vector-oriented simulations we target such calculations to be executed for several particles per call instead of just one, by introducing a similar function with a slightly modified signature:</p>
<pre><code>void contains_v(double const* __restrict__ points,
                bool * __restrict__ isin, int np)
{
   for(int k=0; k&lt;np; k++)
     isin[k] = contains( &amp;points[3*k] );
}</code></pre>
<p>Ideally, such code additions should be enough to benefit from CPU vector capabilities. Unfortunately, this simple code never auto-vectorizes and no gain in efficiency is obtained although we know that vectorization is possible.</p>
<p>After a series of refactorings (manual loop unrolling, code inlining, conversion of memory access) and compiler hints (<code>restrict</code>) we arrive at code that the compiler agrees to auto-vectorize. However, this approach is very expensive on the developer’s side, already for this tiny example, and offers essentially no guarantee of success.</p>
<h3 id="vectorization-using-intrinsics"><a href="#vectorization-using-intrinsics">Vectorization using intrinsics</a></h3>
<p>Our currently favored approach (given the state of standard C++, i.e. the lack of language support for vectorization) therefore consists of the use of vector intrinsics. We pay a price for this: we have to manually change the loop structure and to manage all local variables in a vector way. The following snippet shows Vc <span class="citation" data-cites="VC">(3)</span> (see also <span class="citation" data-cites="N3759">(4)</span>) code for the above example; similar code could be written using the Intel Cilk Plus array notation <span class="citation" data-cites="IntelCilkPlus">(8)</span>.</p>
<pre><code>void contains_v_Vc( double const* points, bool * isin, int np )
{
  // Architecture dependent &quot;gather&quot; indexes:
  unsigned int const i[4]={0,3,6,9};
  Vc::uint_v const indices(i);

  for(int k = 0; k &lt; np; k += Vc::double_v::Size)
  {
    Vc::double_m mask[3];
    for(int dir=0;dir &lt; 3;dir++)
    {
      Vc::double_v x;
      // Gathers a certain number of coordinates
      // into Vc vector x
      x.gather(&amp;points[3*k+dir], indices);
      x = Vc::abs(x - origin[dir]);
      mask[dir] = (x &lt; boxsize[0]);
    }

    Vc::double_m particleinside;
    particleinside = mask[0] &amp;&amp; mask[1] &amp;&amp; mask[2];
    for(int j=0;j&lt;Vc::double_v::Size;++j) {
      isin[k+j] = particleinside[j];
    }
  }
}</code></pre>
<p>For the Vc example above, using the SSE4 instruction set with GCC version 4.7.2 and compilation flags <code>g++ -funroll-loops -Ofast</code> on this code, a speedup factor of 1.93 was seen compared to the non-vectorized version. This factor is close to the size of the SSE4 vector register, i.e. to the number of input data elements that a single vector operation can act on.</p>
<p>We see similar speedup factors in much more complex and complete algorithms, making a dramatic impact in overall performance of our simulation software.</p>
<h2 id="language-enabled-vectorization"><a href="#language-enabled-vectorization">Language-enabled vectorization</a></h2>
<p>Loop syntax and semantics as proposed by <span class="citation" data-cites="N3734">(9)</span> enable us to write this code in a much more compact and readable way. We have measured similar (higher or lower) performance gains as using Vc. Most currently available implementations of loop language extensions only offer <code>#pragma</code> statements instead of proper keywords. A vector for loop using Intel Cilk Plus <span class="citation" data-cites="IntelCilkPlus">(8)</span> could be expressed as:</p>
<pre><code>void contains_v(double const* points, bool * isin, int np)
{
#pragma simd
  for(int k=0; k&lt;np; k++)
    isin[k]=contains( &amp;points[3*k] );
}</code></pre>
<p>For the vectorization to succeed, this requires <code>contains()</code> to be declared as <em>elemental</em> function (see also <span class="citation" data-cites="N3734">(9)</span>).</p>
<p>This loop is impressively close to traditional C++ code. It encapsulates the vector complexity that is much more exposed in the approach using explicit intrinsics. Nonetheless, the algorithm requires careful design to benefit from vectorization. But once spelled out, it has a very readable form – and unlike current C++ <em>does</em> enable vectorization and optimization through compilers.</p>
<p>In summary, use of vectorization drastically increases the performance of number crunching operations. This is not only visible in example code: it can scale to the bulk of algorithms. Using language extensions (for instance that proposed in <span class="citation" data-cites="N3734">(9)</span>), vectorized algorithms can be expressed in a way that is very compact and at the same time very similar to current C++.</p>
<h1 id="motivation-for-vectorization-in-c"><a href="#motivation-for-vectorization-in-c">Motivation for vectorization in C++</a></h1>
<p>Vectorization support should be offered, and it should be offered as a language feature. The following paragraphs explain our reasoning:</p>
<h2 id="use-past-current-and-future-hardware-efficiently-with-c"><a href="#use-past-current-and-future-hardware-efficiently-with-c">Use past, current and future hardware efficiently, with C++</a></h2>
<p>Non-vectorized code uses about 25% of the power of today’s commodity hardware – and even less of tomorrow’s. It runs instructions on each input data, instead of running an instruction on a vector of input data in the same amount of cycles. As vectorized code is in general more compact (because of the operations on vectors) it usually also improves cache performance.</p>
<h2 id="vectorization-should-not-be-hidden-in-a-library"><a href="#vectorization-should-not-be-hidden-in-a-library">Vectorization should not be hidden in a library</a></h2>
<h3 id="vectorized-library-components"><a href="#vectorized-library-components">Vectorized library components</a></h3>
<p>Having a vectorized set of algorithms as suggested by <span class="citation" data-cites="N3554">(13)</span> is not a generic answer to the underlying problem: currently C++ does not allow to communicate to the compiler that a loop’s requirements for vectorization are fulfilled. For most number crunching applications, vectorized standard library algorithms only help in a tiny fraction of all problems. Changing code to make use of them will often cause additional inefficiencies. The effect of modularization (for instance the call to an elemental function like <code>contains()</code> in the above example) on the ability of the standard algorithms to vectorize are not clear at all. It is generally impossible to construct real world algorithms from a set of constituent elements of a hypothetical vectorized standard library.</p>
<h3 id="vectorizing-library-components"><a href="#vectorizing-library-components">Vectorizing library components</a></h3>
<p>Focusing on a vectorized <code>std::for_each</code> (as for instance suggested by <span class="citation" data-cites="N3554">(13)</span>), the differences between a language-based vector loop and a vectorizing library loop seem purely syntactic, with the vectorized <code>std::for_each</code> allowing to move vectorization into the library. But this neglects additional information that must be provided to the compiler for vectorization of realistic cases, such as inter-loop dependencies (for instance different variable types like linear versus fixed for all iterations), and on the other hand missing guarantees on the sequencing of the iterations: like regular for loops, vectorized for loops have sequencing guarantees – but they are different. And just as with regular for loops, developers will have to rely on these sequencing guarantees for real world algorithms. Unlike in the parallel for loop case, the sequencing is non-linear. We find this a change too dramatic to be hidden in a library function.</p>
<p>Even if means were provided to convey additional information to a vectorized <code>std::for_each</code>, the notion of elemental function will have to be added to enable modularity. And this is a language feature. Furthermore, while <code>std::for_each</code> looks like a library-only approach it requires compiler support: only the compiler can convey the information needed from the iterations’ body to the vectorization engine. As an example, at the point of invocation of <code>std::for_each</code> the body of the function argument to <code>std::for_each</code> must be available to the compiler to be able to vectorize it. Thus, much of the advantage of a library solution is purely perceptional.</p>
<h3 id="library-components-wrapping-vector-intrinsics"><a href="#library-components-wrapping-vector-intrinsics">Library components wrapping vector intrinsics</a></h3>
<p>Intrinsics-wrapping vectorization libraries on the other hand suffer from (at least implicit) target architecture dependencies, valarray-style changes to algorithms and the fact that compilers are seemingly unable to optimize across intrinsic calls. Just as we prefer writing C++ over assembler code we should be able to rely on compilers to generate optimal machine code.</p>
<h2 id="prevent-escape-to-non-standard-extensions"><a href="#prevent-escape-to-non-standard-extensions">Prevent escape to non-standard extensions</a></h2>
<p>To leverage the performance of current hardware, people resort to extensions of C++. Those have the usual issue of standard extensions: they are not standard, and where they are (OpenMP might be seen as such) not all C++ compiler vendors implement them – which makes them nonstandard <em>for C++ users</em>.</p>
<h1 id="infrequently-used-counterarguments"><a href="#infrequently-used-counterarguments">Infrequently used counterarguments</a></h1>
<p>Just for completeness, we have collected a few arguments against vectorization support, and our thoughts about those.</p>
<ul>
<li><p><strong>Explicit vectorization annotation is not needed; auto-vectorization will get there eventually.</strong></p>
<p>All compiler vendors we have talked to disagree.</p></li>
<li><p><strong>Explicit vectorization annotation is dangerous; it cannot be correct, and if it is correct now it might become incorrect after a code change.</strong></p>
<p>The same holds true with thread safety: the compiler cannot guarantee thread safety, it has to trust the developer. Just because you have a powerful tool doesn’t mean everyone should use it. In fact the problem is really that currently <em>nobody</em> can write code leveraging vector hardware (almost all current commodity chips) in standard C++.</p></li>
<li><p><strong>A call to any function will cause havoc.</strong></p>
<p>The proposal <span class="citation" data-cites="N3734">(9)</span> (that we enthusiastically support) does not suffer from this. It does not require a “contagious” annotation run through a call graph. It merely uses calls to non-vectorizable functions as “synchronization” points (in the parallel language).</p></li>
<li><p><strong>A call to any (for instance math) function will remove the advantage of vectorized code.</strong></p>
<p>Many libraries exist that re-implement mathematical functions in a vectorizable way <span class="citation" data-cites="INTELSVML AMDLIBM">(14, 15)</span>; we have our own <span class="citation" data-cites="VDT">(16)</span>.</p></li>
</ul>
<h1 id="conclusion"><a href="#conclusion">Conclusion</a></h1>
<p>We have shown that C++ users currently pay a hefty price for using standard C++. Many extensions exist to leverage vector units in a meaningful way. C++ should react; a language extension is needed.</p>
<p>The proposal <span class="citation" data-cites="N3734">(9)</span> goes into the right direction. It would allow us to express our vectorized code as standard compliant code while maintaining its efficiency. Alternative proposals, for instance on an array notation in the spirit of Cilk Plus <span class="citation" data-cites="IntelCilkPlus">(8)</span>, would be interesting to see and discuss. Whatever solution chosen, vectorization support will have a big impact for all (floating point and integer) number crunching communities.</p>
<h1 id="references"><a href="#references">References</a></h1>
<p>1. GEVA, Robert. <em>On the difference between parallel loops and vector loops</em>. N3735</p>
<p>2. ESTERIE, Pierre, GAUNARD, Mathias and FALCOU, Joel. <em>A Proposal to add Single Instruction Multiple Data Computation to the Standard Library</em>. N3561</p>
<p>3. KRETZ, Matthias and LINDENSTRUTH, Volker. Vc: A C++ library for explicit vectorization. <em>Softw: Pract. Exper.</em> 2012. No. 42. </p>
<p>4. KRETZ, Matthias. <em>SIMD Vector Types</em>. N3759</p>
<p>5. MICROSOFT. <em>Visual Studio 2012 C/C++ Preprocessor Reference: pragma loop</em> [online]. Available from: <a href="http://msdn.microsoft.com/en-us/library/hh923901.aspx" title="http://msdn.microsoft.com/en-us/library/hh923901.aspx">http://msdn.microsoft.com/en-us/library/hh923901.aspx</a></p>
<p>6. INTEL. <em>Intel(R) C++ Compiler 12.1 User and Reference Guides: pragma ivdep</em> [online]. Available from: <a href="http://software.intel.com/sites/products/documentation/studio/composer/en-us/2011Update/compiler_c/cref_cls/common/cppref_pragma_ivdep.htm" title="http://software.intel.com/sites/products/documentation/studio/composer/en-us/2011Update/compiler_c/cref_cls/common/cppref_pragma_ivdep.htm">http://software.intel.com/sites/products/documentation/studio/composer/en-us/2011Update/compiler_c/cref_cls/common/cppref_pragma_ivdep.htm</a></p>
<p>7. OPENMP. <em>The OpenMP API specification for parallel programming</em> [online]. Available from: <a href="http://openmp.org/wp/openmp-specifications/" title="http://openmp.org/wp/openmp-specifications/">http://openmp.org/wp/openmp-specifications/</a></p>
<p>8. INTEL. <em>Cilk Plus</em> [online]. Available from: <a href="https://www.cilkplus.org/" title="https://www.cilkplus.org/">https://www.cilkplus.org/</a></p>
<p>9. GEVA, Robert. <em>Vector Programming; A proposal for WG21</em>. N3734</p>
<p>10. TITLE, Analog Devices. [online]. Available from: <a href="http://www.analog.com/en/dsp-software/vdsp-bf-sh-ts/sw.html" title="http://www.analog.com/en/dsp-software/vdsp-bf-sh-ts/sw.html">http://www.analog.com/en/dsp-software/vdsp-bf-sh-ts/sw.html</a></p>
<p>11. DONG, Xin, COOPERMAN, Gene, APOSTOLAKIS, John, JARP, Sverre, NOWAK, Andrzej, ASAI, Makoto and BRANDT, Daniel. Creating and Improving Multi-Threaded Geant4. <em>J. Phys.: Conf. Ser.</em> 2012. No. 396. </p>
<p>12. APOSTOLAKIS, John, BRUN, Rene, CARMINATI, Federico and GHEATA, Andrei. Rethinking particle transport in the many-core era towards GEANT 5. <em>J. Phys.: Conf. Ser.</em> 2012. No. 396. </p>
<p>13. HOBEROCK, Jared et al. <em>A Parallel Algorithms Library</em>. N3554</p>
<p>14. INTEL. <em>Short Vector Math Library (SVML) Functions</em> [online]. Available from: <a href="http://software.intel.com/sites/products/documentation/doclib/iss/2013/compiler/cpp-lin/GUID-7580634E-88D9-4B67-94ED-8472CCC5AF68.htm" title="http://software.intel.com/sites/products/documentation/doclib/iss/2013/compiler/cpp-lin/GUID-7580634E-88D9-4B67-94ED-8472CCC5AF68.htm">http://software.intel.com/sites/products/documentation/doclib/iss/2013/compiler/cpp-lin/GUID-7580634E-88D9-4B67-94ED-8472CCC5AF68.htm</a></p>
<p>15. AMD. <em>LibM</em> [online]. Available from: <a href="http://developer.amd.com/tools-and-sdks/cpu-development/libm/" title="http://developer.amd.com/tools-and-sdks/cpu-development/libm/">http://developer.amd.com/tools-and-sdks/cpu-development/libm/</a></p>
<p>16. PIPARO, Danilo, INNOCENTE, Vincenzo and HAUTH, Thomas. <em>The VDT Mathematical Library</em> [online]. Available from: <a href="https://svnweb.cern.ch/trac/vdt" title="https://svnweb.cern.ch/trac/vdt">https://svnweb.cern.ch/trac/vdt</a></p>
</body>
</html>
